/*
 * Control.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#include "Control.hpp"
#include "ControlFactory.hpp"

std::vector<char> key2char =
		{ '\0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6',
				'7', '8', '9', '\x1', '\x2', '\x3', '\x4', '\x5', '\x6', '\x7', '\x8', '\x9', '\xa', '\xb', '\xc', '\xd', '\xe', '\xf', '\x10', ' ', '\t', '\x11', '\x12', '\x13',
				'\x14', '\x15', '\x16', '\x17', '\x18', '\b', '\x1f', '\r', '\n', '\x19', '\x1a', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '/', '+', '-', '.', };

/*char keyToChar(olc::Key key, bool shift)
 {
 if (shift) {
 return toupper(key2char.at(key));
 } else {
 return tolower(key2char.at(key));
 }
 }*/

/*std::vector<olc::Pixel> Control::palette = { 0xFF000000, 0xFFAA0000, 0xFF00AA00, 0xFFAAAA00, 0xFF0000AA, 0xFFAA00AA, 0xFF0055AA, 0xFFAAAAAA, 0xFF555555, 0xFFFF5555, 0xFF55FF55,
 0xFFFFFF55, 0xFF5555FF, 0xFFFF55FF, 0xFF55FFFF, 0xFFFFFFFF, 0x0 };*/

void Control::DrawFrame(Context * ctx, int x0, int y0, int w0, int h0, int depth) {
	auto olc = ctx->Gfx();
	int x = x0;
	int y = y0;
	int w = w0 - 1;
	int h = h0 - 1;
	switch (depth) {
	case -1:
		olc->DrawLine(x, y, x + w, y, 0);
		olc->DrawLine(x, y, x, y + h, 0);

		olc->DrawLine(x + w, y, x + w, y + h, 15);
		olc->DrawLine(x, y + h, x + w, y + h, 15);

		olc->DrawLine(x + 1, y + 1, x + w - 1, y + 1, 8);
		olc->DrawLine(x + 1, y + 1, x + 1, y + h - 1, 8);
		break;
	case 0:
		olc->DrawRect(x, y, w - 1, h - 1, 0);
		break;
	case 1:
		olc->DrawLine(x + w, y, x + w, y + h, 0);
		olc->DrawLine(x, y + h, x + w, y + h, 0);

		olc->DrawLine(x, y, x + w, y, 15);
		olc->DrawLine(x, y, x, y + h, 15);

		olc->DrawLine(x + 1, y + h - 1, x + w - 1, y + h - 1, 8);

		olc->DrawLine(x + w - 1, y + 1, x + w - 1, y + h - 1, 8);
		break;
	}
}

Control::Control(int x0, int y0, int w0, int h0) :
		x0(x0), y0(y0), w0(w0), h0(h0), value(0.0), text(""), color(7) {
}

void Control::Update(Context * ctx) {
	auto olc = ctx->Gfx();
	if (keyPressed != nullptr) {
		/*for (olc::Key key = olc::Key::A; key < olc::Key::NP_DECIMAL; key++) {
		 if (olc->GetKey(key).bPressed) {
		 keyDown(key);
		 }
		 }*/
	}
	// Emit a clicked event if clicked point to something and mouse is released over control
	if (olc->GetMouse(0).bReleased) {
		int mx = olc->GetMouseX();
		int my = olc->GetMouseY();
		if ((mx >= x0) && (mx < x0 + w0)) {
			if ((my >= y0) && (my < y0 + h0)) {
				if (mouseDown != nullptr)
					mouseDown(mx, my, 0);
			}
		}
	}
}

void Control::KeyPressed(Key key, int mx, int my) {
	// emit a keyDown event if keyDown is set to a handler
	if (keyPressed != nullptr) {
		keyPressed(key, mx, my);
	}
}

void Control::KeyReleased(Key key, int mx, int my) {
	if (keyReleased != nullptr) {
		keyReleased(key, mx, my);
	}
}

void Control::MouseDown(int mx, int my, int button) {
	if (mouseDown != nullptr) {
		mouseDown(mx, my, button);
	}
}

void Control::MouseUp(int mx, int my, int button) {
	if (mouseUp != nullptr) {
		mouseUp(mx, my, button);
	}
}

void Control::TextEntered(char c) {
	if(textEntered != nullptr) {
		textEntered(c);
	}
}
/*void Control::DrawBitmap(Context * ctx, const Bitmap& bmp, int x, int y)
 {

 for (int i = 0; i < bmp.Height(); i++) {
 for (int j = 0; j < bmp.Width(); j++) {
 olc->Draw(x+j, y+i, bmp(j,i));
 }
 }
 }*/
