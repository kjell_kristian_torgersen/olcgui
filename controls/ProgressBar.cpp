/*
 * ProgressBar.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#include "ProgressBar.hpp"

ProgressBar::ProgressBar(int x, int y, int w, int h, double value) : Control(x,y,w,h)
{
	this->value = value;
}

void ProgressBar::Update(Context * ctx)
{
	auto olc = ctx->Gfx();
	// Saturate value in range 0 to 1
	if(value < 0) value = 0;
	if(value > 1) value = 1;

	// Draw background color
	olc->DrawRect(x0, y0, w0-1, h0-1, color, true);

	// Draw a frame around
	DrawFrame(ctx, x0, y0, w0-1, h0-1, -1);

	// Saturate the progressbar
	int ww = (int)(w0*value)-2;
	if(ww < 0) ww = 0;

	// Draw the progressbar
	olc->DrawRect(x0+1, y0+1, ww, h0-3, 1, true);

	// Print the percentage of progress
	std::string percent = std::to_string((int)(100.0*value + 0.5)) + " %";
	olc->DrawString(x0+w0/2-percent.size()*8/2, y0+h0/2-8/2, percent, value > 0.5 ? 1 : 0);
}

ProgressBar::~ProgressBar()
{
	// TODO Auto-generated destructor stub
}

