/*
 * Palette.cpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */

#include "Palette.h"


Palette::Palette(int x, int y, int w, int h) : Control(x,y,w,h), color(1) {

}

void Palette::Update(Context * ctx) {
	auto olc = ctx->Gfx();
	int stepy = h0 / 17;

	int py = (olc->GetMouseY() - y0) / stepy;

	olc->DrawRect(x0, y0+color*stepy, w0, stepy, 0, true);
	for(int i = 0; i < 17; i++) {
		olc->DrawRect(x0+4, y0+i*stepy+4, w0-8, stepy-8, i, true);
	}
	if(olc->GetMouse(0).bPressed) {
		int mx = olc->GetMouseX()-x0;
		if(mx>=0 && mx < w0) {
			if(py>=0 && py<17) {
				color=py;
			}
		}

	}
}

Palette::~Palette() {
	// TODO Auto-generated destructor stub
}

