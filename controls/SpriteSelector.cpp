/*
 * SpriteSelector.cpp
 *
 *  Created on: Jul 28, 2019
 *      Author: kjell
 */

#include "SpriteSelector.hpp"
#include <cmath>

/*olc::Sprite SpriteSelector::BitmapToSprite(const Bitmap& bmp)
 {
 // Create the sprite of correct size
 auto sprite = olc::Sprite(bmp.Width(), bmp.Height());

 // Write each pixel according to palette
 for (uint32_t y = 0; y < bmp.Height(); y++) {
 for (uint32_t x = 0; x < bmp.Width(); x++) {
 if (bmp(x, y) >= 0 && bmp(x, y) < 17) {
 sprite.SetPixel(x, y, palette.at(bmp(x, y)));
 } else {
 sprite.SetPixel(x, y, olc::DARK_MAGENTA);
 }
 }
 }
 return sprite;
 }*/

SpriteSelector::SpriteSelector(int x, int y, int w, int h) :
		Control(x, y, w, h), selected(0) {
}

void SpriteSelector::Update(Context * ctx) {
	auto olc = ctx->Gfx();
	if (olc->Sprites().size() != 0) {
		olc->Clear(0);
		//if (sprites != nullptr) {
		int nw = sqrt(olc->Sprites().size());
		int sw = olc->Sprites().at(0).Width();
		int sh = olc->Sprites().at(0).Height();

		int mx = (olc->GetMouseX() - x0) / sw;
		int my = (olc->GetMouseY() - y0) / sh;

		for (unsigned i = 0; i < olc->Sprites().size(); i++) {
			int x = (i % nw) * sw;
			int y = (i / nw) * sh;
			olc->DrawSprite(x0 + x, y0 + y, i);
			//DrawBitmap(olc, olc->Sprites().at(i), x0 + x, y0 + y);
			//olc->DrawSprite(x0 + x, y0 + y, &olc->Sprites().at(i));
			//olc->DrawSprite(0, 0, &olc->Sprites().at(i));
		}

		olc->DrawRect(x0 + mx * sw, y0 + my * sh, sw, sh, 4);
		if (olc->GetMouse(0).bPressed) {
			if (my >= 0 && my < nw) {
				if (mx >= 0 && mx < nw) {
					selected = my * nw + mx;
				}
			}
		}
	}
	//}
}

int SpriteSelector::GetSelected() {
	return selected;
}

SpriteSelector::~SpriteSelector() {
	// TODO Auto-generated destructor stub
}

