/*
 * Level.cpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#include "LevelControl.hpp"

LevelControl::LevelControl(int x, int y, int w, int h) :
		Control(x, y, w, h) {
}

void LevelControl::Update(Context * ctx) {
	auto map = ctx->Map();
	auto olc = ctx->Gfx();
	int sw = olc->Sprites().at(0).Width();
	int sh = olc->Sprites().at(0).Height();
	int nx = w0 / sw;
	int ny = h0 / sh;

	for (int y = 0; y < ny; y++) {
		for (int x = 0; x < nx; x++) {
			olc->DrawSprite(x0 + x * sw, y0 + y * sh, map(x, y));
		}
	}
	int mx = (ctx->Gfx()->GetMouseX() - x0) / sw;
	int my = (ctx->Gfx()->GetMouseY() - x0) / sh;

	olc->DrawSprite(x0 + mx * sw, y0 + my * sh, ctx->CurrentTile());

	if (ctx->Gfx()->GetMouse(0).bPressed || ctx->Gfx()->GetMouse(0).bHeld) {

		ctx->Map()(mx, my) = ctx->CurrentTile();
	}
	if (ctx->Gfx()->GetMouse(1).bPressed) {
		ctx->CurrentTile() = ctx->Map()(mx, my);
	}
}

LevelControl::~LevelControl() {
// TODO Auto-generated destructor stub
}

