/*
 * Canvas.cpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */

#include "Canvas.hpp"
#include "BitmapIO.hpp"
#include "LevelControl.hpp"
#include "LevelMap.hpp"
#include <olcPixelGameEngine.h>

Canvas::Canvas(int x, int y, int w, int h, int imageWidth, int imageHeight) :
		Control(x, y, w, h), currentImage(0), label(x, y - 10, w, 20, "(0,0)"), spriteSelector(x, y, w, h), bmp(32, 32), map(16, 16), levelControl(x, y, w, h),
		drawState(x,y,w,h,imageWidth,imageHeight)
{
}

void Canvas::Update(Context* ctx) {
	drawState.Update(ctx);
	/*auto gfx = ctx->Gfx();
	if (gfx->GetKey(F1).bHeld) {
		spriteSelector.Update(ctx);
		//bmp = olc->Sprites().at(spriteSelector.GetSelected());
		ctx->CurrentTile() = spriteSelector.GetSelected();
//		bmp = ctx->Gfx()->Sprites().at(ctx->CurrentTile());
	} else if (gfx->GetKey(F2).bHeld) {
		levelControl.Update(ctx);
	} else {
		DrawMode(ctx);
	}*/
}

void Canvas::SetPixel(int x, int y, int color) {
	//auto& bmp = bitmaps.at(currentImage);
	bmp(x, y) = color;
}

int Canvas::GetPixel(int x, int y) {
	//auto& bmp = bitmaps.at(currentImage);
	return bmp(x, y);
}

void Canvas::FloodFill(int x, int y, int oldColor, int newColor) {
	//auto& bmp = bitmaps.at(currentImage);
	if (x < 0 || y < 0)
		return;
	if (x >= bmp.Width() || y >= bmp.Height())
		return;
	if (GetPixel(x, y) == newColor)
		return;
	if (GetPixel(x, y) != oldColor)
		return;
	SetPixel(x, y, newColor);
	FloodFill(x, y - 1, oldColor, newColor);
	FloodFill(x, y + 1, oldColor, newColor);
	FloodFill(x - 1, y, oldColor, newColor);
	FloodFill(x + 1, y, oldColor, newColor);
}

void Canvas::KeyPressed(Key key, int mx, int my) {
	//auto& bmp = bitmaps.at(currentImage);
	// React on key presses
	switch (key) {
	case A:
		clipboard = bmp;
		break;
	case B:
		undo.push(bmp);
		bmp = clipboard;
		break;
	case F: {
		undo.push(bmp);
		int stepx = w0 / bmp.Width();
		int stepy = h0 / bmp.Height();
		int px = (mx - x0) / stepx;
		int py = (my - y0) / stepy;
		Fill(bmp, px, py, bmp(px, py), cpalette->color);
		//FloodFill(px, py, GetPixel(px, py), cpalette->color);
	}
		break;
	case C:
		undo.push(bmp);
		bmp.Clear(cpalette->color);
		break;
	case U:
		if (undo.size() != 0) {
			bmp = undo.top();
			undo.pop();
		}
		break;
	case S:
		//BitmapToFile("sprites.bin", olc.);
		break;
	case L:
		/*bitmaps = BitmapFromFile("sprites.bin");
		 while (!undo.empty())
		 undo.pop();*/
		break;
	case O:
		bmp = Offset(bmp, bmp.Width() / 2, bmp.Height() / 2);
		break;
	default:
		break;
	}
}

Canvas::~Canvas() {
	// TODO Auto-generated destructor stub
}

