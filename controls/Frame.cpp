/*
 * Frame.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#include "Frame.h"


Frame::Frame(int x, int y, int w, int h, int depth) : Control(x,y,w,h), depth(depth)
{
}

void Frame::Update(Context * ctx)
{
	ctx->Gfx()->DrawRect(x0, y0, w0-1, h0-1, 7, true);
	DrawFrame(ctx, x0, y0, w0-1, h0-1, depth);
}

Frame::~Frame()
{
	// TODO Auto-generated destructor stub
}

