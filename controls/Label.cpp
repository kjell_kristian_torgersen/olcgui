/*
 * Label.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#include "Label.h"

Label::Label(int x, int y, int w, int h, const std::string& label) :
		Control(x, y, w, h)
{
	this->text = label;
}

void Label::Update(Context * ctx)
{
	auto olc = ctx->Gfx();
	olc->DrawRect(x0, y0, w0 - 1, h0 - 1, color, true);

	//this->DrawFrame(olc, x, y, w, h, -1);

	olc->DrawString(x0 + 3, y0 + 3, text, 0);
}

Label::~Label()
{
	// TODO Auto-generated destructor stub
}

