/*
 * TextBox.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#include "TextBox.hpp"

TextBox::TextBox(int x, int y, int w, int h, const std::string& text) :
		Control(x, y, w, h)
{
	this->text = text;
	this->color = 15;
}

void TextBox::Update(Context * ctx)
{
	//int mx = olc->GetMouseX();
	//int my = olc->GetMouseY();
	Control::Update(ctx);

	/*bool mouseOver = false;
	 if (mx >= x && my < x + w) {
	 if (my >= y && my < y + h) {
	 mouseOver = true;
	 }
	 }*/
	auto olc = ctx->Gfx();
	ctx->Gfx()->DrawRect(x0, y0, w0 - 1, h0 - 1,color, true);

	this->DrawFrame(ctx, x0, y0, w0, h0, -1);

	olc->DrawString(x0 + 3, y0 + 3, text, 0);
}

TextBox::~TextBox()
{
	// TODO Auto-generated destructor stub
}

void TextBox::KeyPressed(Key key, int mx, int my)
{
	// React on key presses
	if (keyPressed) keyPressed(key, mx, my);
	switch (key) {
	case 0x8:
		text = text.substr(0, text.size() - 1);
		break;
	default:
		text += key;
		break;
	}
}
