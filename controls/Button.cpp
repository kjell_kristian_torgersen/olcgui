/*
 * Button.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#include "Button.hpp"

Button::Button(int x, int y, int w, int h, const std::string& label) :
		Control(x, y, w, h)
{
	this->text = label;
	this->value = 0.0;
}

void Button::Update(Context * ctx)
{
	auto olc = ctx->Gfx();
	// get the mouse position
	int mx = olc->GetMouseX();
	int my = olc->GetMouseY();

	// run update on overridden Update
	Control::Update(ctx);

	// Check if mouse is over button
	bool mouseOver = false;
	if ((mx >= x0) && (mx < x0 + w0)) {
		if ((my >= y0) && (my < y0 + h0)) {
			mouseOver = true;
		}
	}

	// Draw the button color
	olc->DrawRect(x0, y0, w0 - 1, h0 - 1, mouseOver ? color : color, true);

	// Animate button click (clicking is handled in Control::Update()
	if (mouseOver && (olc->GetMouse(0).bHeld || olc->GetMouse(0).bPressed)) {
		this->DrawFrame(ctx, x0, y0, w0, h0, -1);
		// Draw label
		//olc->DrawString(x0 + w0 / 2 - text.size() * 8 / 2+1, y0 + h0 / 2 - 8 / 2+1, text, 0);
		olc->DrawString(x0 + w0 / 2 - text.size() * 8 / 2+1, y0 + h0 / 2 - 1, text, 0);
	} else {
		this->DrawFrame(ctx, x0, y0, w0, h0, 1);
		// Draw label
		olc->DrawString(x0 + w0 / 2 - text.size() * 8 / 2, y0 + h0 / 2 , text, 0);
	}

}
