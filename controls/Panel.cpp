/*
 * Panel.cpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */

#include "Panel.hpp"
#include <sstream>
#include "ControlFactory.hpp"
#include <olcPixelGameEngine.h>

std::vector<std::string> splitstring(const std::string& str, char delim = ' ')
{
	std::vector<std::string> cont;
	std::stringstream ss(str);
	std::string token;
	while (std::getline(ss, token, delim)) {
		cont.push_back(token);
	}
	return cont;
}

Panel::Panel(int x, int y, int w, int h) :
		Control(x, y, w, h)
{
}

void Panel::Update(Context * ctx)
{
	auto olc = ctx->Gfx();
	int mx = olc->GetMouseX();
	int my = olc->GetMouseY();

	// Handle if a control is focused. Focused controls receive keyboard input
	if (focused != nullptr) {
		//bool shift = olc->GetKey(SHIFT).bHeld;
		/*bool shift = false;
		for (int i = A; i <= Z; i++) {
			enum Key key = (enum Key) i;
			if (olc->GetKey(key).bPressed) {
				char c = 'A' + i - A;
				focused->KeyDown(focused, shift ? c : tolower(c), mx, my);
			}
		}

		for (int i = Num0; i <= Num9; i++) {
			enum Key key = (enum Key) i;
			if (olc->GetKey(key).bPressed) {
				char c = '0' + i - Num0;
				focused->KeyDown(focused, c, mx, my);
			}
		}

		for (int i = Numpad0; i <= Numpad0; i++) {
			enum Key key = (enum Key) i;
			if (olc->GetKey(key).bPressed) {
				char c = '0' + i - Numpad0;
				focused->KeyDown(focused, c, mx, my);
			}
		}
		if (olc->GetKey(Delete).bPressed || olc->GetKey(BackSpace).bPressed) {
			focused->KeyDown(focused, 0x8, mx, my);
		}
		if (olc->GetKey(Space).bPressed) {
			focused->KeyDown(focused, ' ', mx, my);
		}
		if (olc->GetKey(Add).bPressed) {
			focused->KeyDown(focused, '+', mx, my);
		}
		if (olc->GetKey(Subtract).bPressed) {
			focused->KeyDown(focused, '-', mx, my);
		}
		if (olc->GetKey(Multiply).bPressed) {
			focused->KeyDown(focused, '*', mx, my);
		}
		if (olc->GetKey(Divide).bPressed) {
			focused->KeyDown(focused, '/', mx, my);
		}*/
	}

	// Let all other controls react on events from the pixel game engine, set focused control if clicked on
	for (auto&control : controls) {
		if (olc->GetMouse(0).bPressed) {
			if (mx >= control.second->x0 && mx < control.second->x0 + control.second->w0) {
				if (my >= control.second->y0 && my < control.second->y0 + control.second->h0) {
					focused = control.second;
				}
			}
		}
		control.second->Update(ctx);
	}
}

Panel::~Panel()
{
	for (auto&control : controls) {
		delete (control.second);
	}
}

// Load a set of controls from a text file
Panel* Panel::FromFile(const std::string& fileName, int n)
{
	Panel * ret = new Panel();
	std::string line;
	std::ifstream myfile(fileName);
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			if ((line.at(0) != '#') && line.size() != 0) {
				auto split = splitstring(line, ';');
				if (split.size() >= 8) {
					//std::cerr << line << std::endl;
					std::string control = split.at(0);
					std::string name = split.at(1);
					int x = std::stoi(split.at(2));
					int y = std::stoi(split.at(3));
					int w = std::stoi(split.at(4));
					int h = std::stoi(split.at(5));
					double value = std::stod(split.at(6));
					std::string text = split.at(7);
					ret->controls.insert( { name, Control_Create(control, n * x + 1, n * y + 1, n * w - 2, n * h - 2, value, text) });
				} else {
					std::cerr << "Syntax error: " << line << std::endl;
				}
			}
		}
		myfile.close();
	}

	else std::cout << "Unable to open file";
	return ret;
}

// Manually add a control to a panel
void Panel::AddControl(const std::string& name, Control* control)
{
	controls.insert( { name, control });
}

// Get access to a control by name
Control* Panel::GetControl(const std::string& name)
{
	if (controls.count(name) == 0) {
		std::cerr << "Error accessing control " << name << std::endl;
		return nullptr;
	} else {
		return controls.at(name);
	}
}
