/*
 * Game.hpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#ifndef GAME_HPP_
#define GAME_HPP_

#include "IGraphics.hpp"
#include "Panel.hpp"
#include "LevelControl.hpp"
#include "LevelMap.hpp"
#include "DrawState.hpp"
#include "SelectTileState.hpp"
#include "LevelEditState.hpp"

class Game {
public:
	Game();
	bool Update(Context * gfx, double dt);
	void SetGraphics(Context * ctx);
	virtual ~Game();
private:
	IState * currentState;
	DrawState drawState;
	SelectTileState selectTileState;
	LevelEditState levelEditState;
};

#endif /* GAME_HPP_ */
