/*
 * SelectTileState.hpp
 *
 *  Created on: Aug 3, 2019
 *      Author: kjell
 */

#ifndef SELECTTILESTATE_HPP_
#define SELECTTILESTATE_HPP_
#include "IState.hpp"

class SelectTileState : public IState {
public:
	SelectTileState();
	virtual bool Update(Context * ctx) override;
	virtual void KeyPressed(Context * ctx, Key key, int mx, int my) override;
	virtual void KeyReleased(Context * ctx, Key key, int mx, int my) override;
	virtual void MouseDown(Context * ctx, int mx, int my, int button) override;
	virtual void MouseUp(Context * ctx, int mx, int my, int button) override;
	virtual void TextEntered(Context * ctx, char c) override;
	virtual ~SelectTileState();
};

#endif /* SELECTTILESTATE_HPP_ */
