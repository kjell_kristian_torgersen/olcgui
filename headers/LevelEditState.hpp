/*
 * LevelEditState.hpp
 *
 *  Created on: Aug 3, 2019
 *      Author: kjell
 */

#ifndef LEVELEDITSTATE_HPP_
#define LEVELEDITSTATE_HPP_

#include <IState.hpp>

class LevelEditState: public IState {
public:
	LevelEditState();
	virtual bool Update(Context * ctx) override;
	virtual void KeyPressed(Context * ctx, Key key, int mx, int my) override;
	virtual void KeyReleased(Context * ctx, Key key, int mx, int my) override;
	virtual void MouseDown(Context * ctx, int mx, int my, int button) override;
	virtual void MouseUp(Context * ctx, int mx, int my, int button) override;
	virtual void TextEntered(Context * ctx, char c) override;
	virtual ~LevelEditState();
};

#endif /* LEVELEDITSTATE_HPP_ */
