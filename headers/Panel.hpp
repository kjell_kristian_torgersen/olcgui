/*
 * Panel.hpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */

#ifndef PANEL_HPP_
#define PANEL_HPP_

#include <Control.hpp>
#include <map>
#include <memory>

// A panel is a collection of controls and handling of mouse and keyboard, focused controls and so on
class Panel: public Control {
public:
	Panel(int x=0, int y=0, int w=0, int h=0);
	static Panel * FromFile(const std::string& fileName, int n = 20);
	virtual void Update(Context * ctx) override;
	virtual ~Panel();
	void AddControl(const std::string& name, Control * control);
	Control * GetControl(const std::string& name);
	Control * focused = nullptr;
private:
	// just to get access to them all. use unique_ptr so they automatically get deleted
	std::map<std::string, Control*> controls;
};

#endif /* PANEL_HPP_ */
