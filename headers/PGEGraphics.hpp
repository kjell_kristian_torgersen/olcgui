/*
 * PGEGraphics.hpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#ifndef PGEGRAPHICS_HPP_
#define PGEGRAPHICS_HPP_

#include "IGraphics.hpp"
#include "PGE.hpp"
#include "Game.hpp"

class PGEGraphics: public IGraphics {
public:
	PGEGraphics(int w, int h, Game * game);
	virtual void DrawPixel(int x, int y, int color) override;
	virtual HWButton GetKey(Key key) override;
	virtual void Clear(int color = 0) override;
	//virtual void FillRect(int x, int y, int w, int h, int color) override;
	virtual void DrawRect(int x, int y, int w, int h, int color,bool fill=false) override;
	virtual HWButton GetMouse(int button) override;
	virtual void DrawLine(int x0, int y0, int x1, int y2, int color) override;
	virtual void DrawString(int x, int y, const std::string& text, int color) override;
	virtual void DrawSprite(int x, int y, int sprite) override;
	virtual int GetMouseX() override;
	virtual int GetMouseY() override;
	virtual int ScreenWidth() override;
	virtual int ScreenHeight() override;
	virtual std::function<void(Context*, Key, int, int)>& KeyDown() override;
	virtual std::function<void(Context*, Key , int, int)>& KeyUp() override;
	virtual std::function<void(Context*, int, int, int)>& MouseDown() override;
	virtual std::function<void(Context*, int, int, int)>& MouseUp() override;
	virtual std::function<void(Context*, char)>& TextEntered() override;
	bool Update(float fElapsedTime);
	virtual std::vector<Bitmap>& Sprites();
	virtual ~PGEGraphics();
private:
	PGE pge;
	Game * game;
	std::vector<Bitmap> sprites;
	Context ctx;
	std::function<void(Context*, Key, int, int)> keyDown;
	std::function<void(Context*, Key, int, int)> keyUp;
	std::function<void(Context*, int, int, int)> mouseDown;
	std::function<void(Context*, int, int, int)> mouseUp;
	std::function<void(Context*, char)> textEntered;
};

#endif /* PGEGRAPHICS_HPP_ */
