/*
 * SpriteSelector.hpp
 *
 *  Created on: Jul 28, 2019
 *      Author: kjell
 */

#ifndef SPRITESELECTOR_HPP_
#define SPRITESELECTOR_HPP_

#include <Control.hpp>
#include <vector>
#include "Bitmap.hpp"

class SpriteSelector: public Control {
public:
	SpriteSelector(int x, int y, int w, int h);
	//static olc::Sprite BitmapToSprite(const Bitmap& bmp);
	virtual void Update(Context * ctx) override;
	int GetSelected();
	virtual ~SpriteSelector();
private:
	int selected;
};

#endif /* SPRITESELECTOR_HPP_ */
