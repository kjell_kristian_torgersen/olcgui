/*
 * ProgressBar.hpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#ifndef PROGRESSBAR_HPP_
#define PROGRESSBAR_HPP_

#include <Control.hpp>

// A normal progressbar
class ProgressBar: public Control {
public:
	ProgressBar(int x, int y, int w, int h, double value=0);
	virtual void Update(Context * ctx);
	virtual ~ProgressBar();
};

#endif /* PROGRESSBAR_HPP_ */
