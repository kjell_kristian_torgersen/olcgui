#ifndef __BITMAP__
#define __BITMAP__

#include <cstdint>
#include <vector>
#include <string>

class Bitmap {
public:
	// Constructor 
	Bitmap(int width=32, int height=32);

	const uint8_t & operator()(uint32_t x, uint32_t y) const;
	uint8_t & operator()(uint32_t x, uint32_t y);

	// Getters 
	int Width() const;
	int Height() const;
	std::vector<uint8_t> & Pixels();

	void Clear(int color);

	// Destructor 
	virtual ~Bitmap();

private:
	int width;
	int height;
	std::vector<uint8_t> pixels;
};

std::ostream& operator<<(std::ostream& os, const Bitmap& bitmap);
#endif // __BITMAP__
