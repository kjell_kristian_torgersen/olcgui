/*
 * DrawState.hpp
 *
 *  Created on: Aug 3, 2019
 *      Author: kjell
 */

#ifndef DRAWSTATE_HPP_
#define DRAWSTATE_HPP_

#include <stack>

#include "Label.h"
#include "IState.hpp"
#include "Palette.h"

class DrawState: public IState {
public:
	DrawState(int x, int y, int w, int h, int imageWidth = 32, int imageHeight = 32);
	virtual bool Update(Context * ctx) override;
	virtual void KeyPressed(Context * ctx, Key key, int mx, int my) override;
	virtual void KeyReleased(Context * ctx, Key key, int mx, int my) override;
	virtual void MouseDown(Context * ctx, int mx, int my, int button) override;
	virtual void MouseUp(Context * ctx, int mx, int my, int button) override;
	virtual void TextEntered(Context * ctx, char c) override;
	virtual ~DrawState();
private:
	int x0,y0,w0,h0;
	Bitmap bmp;
	Palette palette;
	std::stack<Bitmap> undo;
	Label label;
};

#endif /* DRAWSTATE_HPP_ */
