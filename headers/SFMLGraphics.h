/*
 * SFMLGraphics.h
 *
 *  Created on: Jul 30, 2019
 *      Author: kjell
 */

#ifndef SFMLGRAPHICS_H_
#define SFMLGRAPHICS_H_

#include "IGraphics.hpp"
#include <SFML/Graphics.hpp>
#include "Game.hpp"

class SFMLGraphics: public IGraphics {
public:
	SFMLGraphics(int width, int height, Game * game);
	virtual void DrawPixel(int x, int y, int color) override;
	virtual HWButton GetKey(Key key) override;
	virtual void Clear(int color = 0) override;
	//virtual void FillRect(int x, int y, int w, int h, int color) override;
	virtual void DrawRect(int x, int y, int w, int h, int color, bool fill=false) override;
	virtual HWButton GetMouse(int button) override;
	virtual void DrawLine(int x0, int y0, int x1, int y2, int color) override;
	virtual void DrawString(int x, int y, const std::string& text, int color) override;
	virtual void DrawSprite(int x, int y, int sprite) override;
	virtual int GetMouseX() override;
	virtual int GetMouseY() override;
	virtual int ScreenWidth() override;
	virtual int ScreenHeight() override;
	virtual std::function<void(Context*, Key, int, int)>& KeyDown() override;
	virtual std::function<void(Context*, Key, int, int)>& KeyUp() override;
	virtual std::function<void(Context*, int, int, int)>& MouseDown() override;
	virtual std::function<void(Context*, int, int, int)>& MouseUp() override;
	virtual std::function<void(Context*, char)>& TextEntered() override;
	//virtual bool Update(float fElapsedTime)override;
	virtual std::vector<Bitmap>& Sprites() override;
	virtual ~SFMLGraphics();
private:
	sf::RenderWindow window;
	sf::Texture texture;
	Game * game;
	std::vector<Bitmap> sprites;
	Context ctx;
	std::vector<sf::Color> palette;
	sf::Font font;
	std::vector<HWButton> mouse;
	std::vector<HWButton> keys;
	std::function<void(Context*,Key, int, int)> keyPressed;
	std::function<void(Context*,Key, int, int)> keyReleased;
	std::function<void(Context*,int, int, int)> mouseDown;
	std::function<void(Context*,int, int, int)> mouseUp;
	std::function<void(Context*,char)> textEntered;
	std::vector<sf::Sprite> sfmlsprites;

	void UpdateSprites();
};

#endif /* SFMLGRAPHICS_H_ */
