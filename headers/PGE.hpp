/*
 * PGEGraphics.hpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#ifndef PGE_HPP_
#define PGE_HPP_

#include <olcPixelGameEngine.h>

class PGEGraphics;

class PGE : public olc::PixelGameEngine {
public:
	PGE(int x, int y, PGEGraphics * gfx);
	bool OnUserCreate() override;
	bool OnUserUpdate(float fElapsedTime) override;
	virtual ~PGE();
private:
	PGEGraphics * gfx;
};

#endif /* PGE_HPP_ */
