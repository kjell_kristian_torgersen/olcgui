/*
 * Context.hpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#ifndef CONTEXT_HPP_
#define CONTEXT_HPP_

#include "IGraphics.hpp"
#include "LevelMap.hpp"

class Context {
public:
	Context(IGraphics * gfx);
	virtual ~Context();
	IGraphics* Gfx();
	LevelMap& Map();
	int& CurrentTile();
private:
	IGraphics * gfx;
	LevelMap map;
	int currentTile;
};

#endif /* CONTEXT_HPP_ */
