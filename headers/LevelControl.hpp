/*
 * Level.hpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#ifndef LEVELCONTROL_HPP_
#define LEVELCONTROL_HPP_

#include <Control.hpp>
#include "LevelMap.hpp"

class LevelControl: public Control {
public:
	LevelControl(int x, int y, int w, int h);
	virtual void Update(Context * ctx) override;
	virtual ~LevelControl();
};

#endif /* LEVELCONTROL_HPP_ */
