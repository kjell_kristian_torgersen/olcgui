/*
 * IControl.hpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#ifndef CONTROL_HPP_
#define CONTROL_HPP_

//#include <olcPixelGameEngine.h>
#include <string>
#include <Bitmap.hpp>
#include "IGraphics.hpp"
#include "Context.hpp"
#include <functional>

// Abstract base class for all controls
class Control {
public:
	//static std::vector<olc::Pixel> palette;

	Control(int x0, int y0, int w0, int h0);
	virtual void Update(Context * olc);
	void DrawFrame(Context * ctx, int x, int y, int w, int h, int depth);
	//void DrawBitmap(Context * ctx, const Bitmap& bmp, int x, int y);
	virtual void KeyPressed(Key key, int mx, int my);
	virtual void KeyReleased(Key key, int mx, int my);
	virtual void MouseDown(int mx, int my, int button);
	virtual void MouseUp(int mx, int my, int button);
	virtual void TextEntered(char c) ;
	virtual ~Control() {
	}

	int x0, y0, w0, h0;
	double value;
	std::string text;
	int color;
	std::function<void(Key, int, int)> keyPressed;
	std::function<void(Key, int, int)> keyReleased;
	std::function<void(int, int, int)> mouseDown;
	std::function<void(int, int, int)> mouseUp;
	std::function<void(char)> textEntered;
};

#endif /* CONTROL_HPP_ */
