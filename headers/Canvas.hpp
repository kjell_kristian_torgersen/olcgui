/*
 * Canvas.hpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */

#ifndef CANVAS_HPP_
#define CANVAS_HPP_

#include <Control.hpp>
#include "Palette.h"
#include "Label.h"
#include "Bitmap.hpp"
#include <vector>
#include <stack>
#include "SpriteSelector.hpp"
#include "LevelControl.hpp"
#include "DrawState.hpp"
// A canvas planned to use in a sprite editor
class Canvas: public Control {
public:
	Canvas(int x, int y, int w, int h, int imageWidth = 32, int imageHeight = 32);
	//olc::Sprite BitmapToSprite(const Bitmap& bmp) const;
	virtual void Update(Context * ctx) override;
	virtual void KeyPressed(Key key, int mx, int my) override;
	void SetPixel(int x, int y, int color);
	int GetPixel(int x, int y);
	void FloodFill(int x, int y, int oldColor, int newColor);
	virtual ~Canvas();
	Palette * cpalette = nullptr;
private:
	void DrawMode(Context* olc);

	int currentImage;
	std::stack<Bitmap> undo;
	Label label;
	SpriteSelector spriteSelector;
	//std::vector<olc::Sprite> sprites;
	Bitmap clipboard;
	Bitmap bmp;
	LevelMap map;
	LevelControl levelControl;
	DrawState drawState;
};

#endif /* CANVAS_HPP_ */
