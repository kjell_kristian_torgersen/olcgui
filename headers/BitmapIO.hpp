/*
 * BitmapIO.hpp
 *
 *  Created on: Jul 20, 2019
 *      Author: kjell
 */

#ifndef BITMAPIO_HPP_
#define BITMAPIO_HPP_

#include "Bitmap.hpp"

std::vector<Bitmap> BitmapFromFile(const std::string& filename);
bool BitmapToFile(const std::string& filename, const std::vector<Bitmap>& bmp);

void Fill(Bitmap& bmp, uint32_t x, uint32_t y, uint8_t from, uint8_t to);
Bitmap Offset(const Bitmap& bmp, int x0, int y0);


#endif /* BITMAPIO_HPP_ */
