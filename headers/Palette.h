/*
 * Palette.h
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */

#ifndef PALETTE_H_
#define PALETTE_H_

#include <Control.hpp>

// A palette for the sprite editor
class Palette: public Control {
public:
	Palette(int x,int y,int w,int h);
	virtual void Update(Context * ctx) override;
	virtual ~Palette();
	int color;

};

#endif /* PALETTE_H_ */
