/*
 * IState.hpp
 *
 *  Created on: Aug 3, 2019
 *      Author: kjell
 */

#ifndef ISTATE_HPP_
#define ISTATE_HPP_

#include "Context.hpp"

class IState {
public:
	virtual bool Update(Context * ctx)=0;
	virtual void KeyPressed(Context * ctx, Key key, int mx, int my)=0;
	virtual void KeyReleased(Context * ctx, Key key, int mx, int my)=0;
	virtual void MouseDown(Context * ctx, int mx, int my, int button)=0;
	virtual void MouseUp(Context * ctx, int mx, int my, int button)=0;
	virtual void TextEntered(Context * ctx, char c)=0;
	virtual ~IState() {
	}
};

#endif /* ISTATE_HPP_ */
