/*
 * IGraphics.hpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#ifndef IGRAPHICS_HPP_
#define IGRAPHICS_HPP_

#include <string>
#include <vector>
#include "Bitmap.hpp"
#include "Keys.hpp"
#include <functional>

class Context;

struct HWButton {
	bool bReleased;
	bool bPressed;
	bool bHeld;
};

class IGraphics {
public:
	virtual void Clear(int color = 0)=0;
	virtual void DrawPixel(int x, int y, int color)=0;
	virtual void DrawRect(int x, int y, int w, int h, int color, bool fill = true)=0;
	virtual void DrawLine(int x0, int y0, int x1, int y1, int color)=0;
	virtual void DrawString(int x, int y, const std::string& text, int color)=0;
	virtual void DrawSprite(int x, int y, int sprite)=0;
	virtual int ScreenWidth()=0;
	virtual int ScreenHeight()=0;

	virtual HWButton GetKey(Key key)=0;
	virtual HWButton GetMouse(int button)=0;
	virtual int GetMouseX()=0;
	virtual int GetMouseY()=0;

	virtual std::function<void(Context*, Key, int, int)>& KeyDown()=0;
	virtual std::function<void(Context*, Key, int, int)>& KeyUp()=0;
	virtual std::function<void(Context*, int, int, int)>& MouseDown()=0;
	virtual std::function<void(Context*, int, int, int)>& MouseUp()=0;
	virtual std::function<void(Context*, char)>& TextEntered()=0;

	virtual std::vector<Bitmap>& Sprites()=0;
	virtual ~IGraphics() {
	}
};

#endif /* IGRAPHICS_HPP_ */
