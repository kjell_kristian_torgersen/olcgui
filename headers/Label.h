/*
 * Label.h
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#ifndef LABEL_H_
#define LABEL_H_

#include <Control.hpp>

// A normal label, text with background color
class Label: public Control {
public:
	Label(int x, int y, int w, int h, const std::string& label);
	virtual void Update(Context * ctx) override;
	virtual ~Label();
};

#endif /* LABEL_H_ */
