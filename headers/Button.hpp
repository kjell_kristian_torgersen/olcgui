/*
 * Button.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#ifndef BUTTON_CPP_
#define BUTTON_CPP_

#include "Control.hpp"

// Just a normal button to click on
class Button: public Control {
public:
	Button(int x, int y, int w, int h, const std::string& label);
	virtual void Update(Context * ctx) override;
};

#endif /* BUTTON_CPP_ */
