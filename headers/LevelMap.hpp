/*
 * LevelMap.hpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#ifndef LEVELMAP_HPP_
#define LEVELMAP_HPP_

#include <vector>

class LevelMap {
public:
	LevelMap(int width, int height, int tile = 19);
	int& operator()(int x, int y);
	void Fill(int x, int y, int newTile);
	bool Road(int x, int y);
	void SetRoad(int x, int y, bool value);
	virtual ~LevelMap();
	int Height() const;
	int Width() const;

private:
	void Fill(int x, int y, int newTile, int oldTile);
	int width;
	int height;
	std::vector<int> map;
	std::vector<int> road;
	int err;
};

#endif /* LEVELMAP_HPP_ */
