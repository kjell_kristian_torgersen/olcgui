/*
 * ControlFactory.hpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */

#ifndef CONTROLFACTORY_HPP_
#define CONTROLFACTORY_HPP_

#include "Control.hpp"

#include <map>

void RegisterFactory(std::string control,
		std::function<Control*(int, int, int, int,double value,const std::string& text)> generator);

Control * Control_Create(std::string control, int x, int y, int w, int h,double value,const std::string& text);

#endif /* CONTROLFACTORY_HPP_ */
