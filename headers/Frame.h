/*
 * Frame.h
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#ifndef FRAME_H_
#define FRAME_H_

#include <Control.hpp>

// Just a frame to put around stuff
class Frame: public Control {
public:
	Frame(int x, int y, int w, int h, int depth = 1);
	virtual void Update(Context * ctx) override;
	virtual ~Frame();
	int depth;
};

#endif /* FRAME_H_ */
