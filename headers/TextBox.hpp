/*
 * TextBox.hpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kjell
 */

#ifndef TEXTBOX_HPP_
#define TEXTBOX_HPP_

#include <Control.hpp>

// A normal text box
class TextBox: public Control {
public:
	TextBox(int x, int y, int w, int h, const std::string& text="");
	virtual void Update(Context * ctx) override;
	virtual void KeyPressed(Key key, int mx, int my) override;
	virtual ~TextBox();
};

#endif /* TEXTBOX_HPP_ */
