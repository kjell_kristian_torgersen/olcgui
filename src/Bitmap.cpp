#include <cstdint>
#include <vector>
#include <string>
#include <iostream>

#include "Bitmap.hpp"

// Constructor 
Bitmap::Bitmap(int width, int height) :
		width(width), height(height), pixels(width * height)
{
	for(int i = 0; i < width*height; i++) pixels.at(i) = 16;
}

// Getters 
int Bitmap::Width() const
{
	return this->width;
}

int Bitmap::Height() const
{
	return this->height;
}

std::vector<uint8_t> & Bitmap::Pixels()
{
	return this->pixels;
}

const uint8_t& Bitmap::operator ()(uint32_t x, uint32_t y) const
{
	if ((x < (uint32_t)width) && (y < (uint32_t)height)) {
		return pixels.at(y * width + x);
	}
	return pixels.at(0);
}

uint8_t& Bitmap::operator ()(uint32_t x, uint32_t y)
{
	if ((x < (uint32_t)width) && (y < (uint32_t)height)) {
		return pixels.at(y * width + x);
	}
	return pixels.at(0);
}

void Bitmap::Clear(int color)
{
	for(unsigned i = 0; i < pixels.size(); i++) {
		pixels.at(i) = color;
	}
}

// Destructor 
Bitmap::~Bitmap()
{
}

std::ostream & operator<<(std::ostream & os, const Bitmap& bitmap)
{
	os << "Bitmap {";
	os << " width=" << ((int64_t) bitmap.Width()) << ",";
	os << " height=" << ((int64_t) bitmap.Height()) << ",";
	os << "}";
	return os;
}
