/*
 * BitmapIO.cpp
 *
 *  Created on: Jul 20, 2019
 *      Author: kjell
 */

#include "BitmapIO.hpp"
#include <iostream>
#include <fstream>
#include <vector>

#define MAGIC 0x468f01ad

std::vector<Bitmap> BitmapFromFile(const std::string& filename) {
	uint32_t magic;
	uint16_t width;
	uint16_t height;
	uint16_t n;

	std::ifstream is;
	is.open(filename, std::ios::binary);
	is.read((char*) &magic, 4);
	if (magic != MAGIC) {
		std::cerr << "Error reading " << filename << " magic number was " << magic << " but is supposed to be " << 0x468f01ad << std::endl;
	}
	is.read((char*) &width, 2);
	std::cout << "width: " << width << std::endl;
	is.read((char*) &height, 2);
	std::cout << "height: " << height << std::endl;
	is.read((char*) &n, 2);
	std::cout << "n: " << n << std::endl;

	std::vector<Bitmap> ret;

	for (int i = 0; i < n; i++) {
		int npixels = 0;
		Bitmap bmp(width, height);
		do {

			uint8_t tmp;

			is.read((char*) &tmp, 1);
			int count = (tmp / 17) + 1;
			int color = tmp % 17;

			for (int j = 0; j < count; j++) {
				bmp(npixels % width, npixels / width) = color;
				npixels++;
			}
		} while (npixels < width * height);
		ret.push_back(bmp);
	}
	return ret;
}

bool BitmapToFile(const std::string& filename, const std::vector<Bitmap>& bmp) {
	std::ofstream os;
	uint32_t magic = MAGIC;

	uint16_t width = bmp.at(0).Width();
	uint16_t height = bmp.at(0).Height();
	uint16_t n = bmp.size();
	;

	os.open(filename, std::ios::binary);
	os.write((char*) &magic, 4);
	os.write((char*) &width, 2);
	os.write((char*) &height, 2);
	os.write((char*) &n, 2);

	for (int i = 0; i < n; i++) {
		int color = bmp.at(i)(0, 0);
		int count = 1;
		for (uint32_t y = 0; y < height; y++) {
			for (uint32_t x = 0; x < width; x++) {

				// skip first pixels since it is used to initialize algoritm
				if (x == 0 && y == 0)
					x++;

				if (color != bmp.at(i)(x, y) || count == 15) {
					// new color, write RLE data
					uint8_t tmp = color + ((count - 1) * 17);
					os.write((char*) &tmp, 1);
					color = bmp.at(i)(x, y);
					count = 1;
				} else {
					count++;
				}
			}
		}
		uint8_t tmp = color + ((count - 1) * 17);
		os.write((char*) &tmp, 1);
	}
	return true;
}

void Fill(Bitmap& bmp, uint32_t x, uint32_t y, uint8_t from, uint8_t to) {
	if (x >= (uint32_t) bmp.Width())
		return;
	if (y >= (uint32_t) bmp.Height())
		return;
	if (from == to)
		return;

	if (bmp(x, y) == from) {
		bmp(x, y) = to;
		Fill(bmp, x, y - 1, from, to);
		Fill(bmp, x, y + 1, from, to);
		Fill(bmp, x - 1, y, from, to);
		Fill(bmp, x + 1, y, from, to);
	}
}

Bitmap Offset(const Bitmap& bmp, int x0, int y0) {
	int w = bmp.Width();
	int h = bmp.Height();
	Bitmap tmp(w, h);

	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			tmp(x, y) = bmp((x + x0 + w) % w, (y + y0 + h) % h);
		}
	}
	return tmp;
}
