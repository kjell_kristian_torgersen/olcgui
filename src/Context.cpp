/*
 * Context.cpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#include <iostream>
#include <fstream>

#include "Context.hpp"

#define MAGIC 0xd7c1aa9d

LevelMap MapFromFile(const std::string& filename) {
	uint32_t magic;
	uint16_t width;
	uint16_t height;
	uint8_t n;

	std::ifstream is;
	is.open(filename, std::ios::binary);
	is.read((char*) &magic, 4);
	if (magic != MAGIC) {
		std::cerr << "Error reading " << filename << " magic number was " << magic << " but is supposed to be " << MAGIC << std::endl;
	}
	is.read((char*) &width, 2);
	std::cout << "width: " << width << std::endl;
	is.read((char*) &height, 2);
	std::cout << "height: " << height << std::endl;

	LevelMap map(width, height);

	for (int i = 0; i < width * height; i++) {
		is.read((char*) &n, 1);
		map(i % width, i / width) = n;
	}
	return map;
}

bool MapToFile(const std::string& filename, LevelMap& map) {
	std::ofstream os;
	uint32_t magic = MAGIC;

	uint16_t width = map.Width();
	uint16_t height = map.Height();

	os.open(filename, std::ios::binary);
	os.write((char*) &magic, 4);
	os.write((char*) &width, 2);
	os.write((char*) &height, 2);

	for (int i = 0; i < width * height; i++) {
		char tmp = map(i % width, i / width);
		os.write((char*) &tmp, 1);
	}
	return true;
}

Context::Context(IGraphics* gfx) :
		gfx(gfx), map(MapFromFile("level.bin")), currentTile(0) {

}

Context::~Context() {
	MapToFile("level.bin", map);
}

IGraphics* Context::Gfx() {
	return gfx;
}

LevelMap& Context::Map() {
	return map;
}

int& Context::CurrentTile() {
	return currentTile;
}
