/*
 * LevelMap.cpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#include "LevelMap.hpp"
#include <stdlib.h>

LevelMap::LevelMap(int width, int height, int tile) :
		width(width), height(height), map(width * height), road(width * height), err(255) {
	// TODO Auto-generated constructor stub
	for (int i = 0; i < width * height; i++)
		map[i] = tile;
	/*int x = 10; //width / 2;
	 int y = 10; //height / 2;
	 int angle = 0;
	 for (int u = 0; u < 10; u++) {
	 for (int v = 0; v < 5 + (rand() % 10); v++) {
	 switch (angle % 4) {
	 case 0:
	 x++;
	 break;
	 case 1:
	 y++;
	 break;
	 case 2:
	 x--;
	 break;
	 case 3:
	 y--;
	 break;
	 }
	 SetRoad(x % width, y % height, true);
	 }
	 angle++;
	 }*/
}

LevelMap::~LevelMap() {
	// TODO Auto-generated destructor stub
}

int& LevelMap::operator ()(int x, int y) {
	if (x >= 0 && x < width && y >= 0 && y < height) {
		return map.at(y * width + x);
	} else {
		return err;
	}
}

int LevelMap::Height() const {
	return height;
}

bool LevelMap::Road(int x, int y) {
	if (x >= 0 && x < width) {
		if (y >= 0 && y < height) {
			return road[y * width + x] != 0;
		}
	}
	return false;
}

void LevelMap::SetRoad(int x, int y, bool value) {
	if (x >= 0 && x < width) {
		if (y >= 0 && y < height) {
			road[y * width + x] = value ? 1 : 0;
		}
	}
}

void LevelMap::Fill(int x, int y, int newTile) {
	Fill(x, y, newTile, operator ()(x, y));
}

int LevelMap::Width() const {
	return width;
}

void LevelMap::Fill(int x, int y, int newTile, int oldTile) {
	if (x < 0 || y < 0 || x >= width || y >= height)
		return;
	if (operator ()(x, y) != oldTile)
		return;
	if(oldTile == newTile) return;

	operator ()(x, y) = newTile;
	Fill(x, y - 1, newTile, oldTile);
	Fill(x, y + 1, newTile, oldTile);
	Fill(x - 1, y, newTile, oldTile);
	Fill(x + 1, y, newTile, oldTile);
}
