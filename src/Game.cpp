/*
 * Game.cpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#include "Game.hpp"
#include "Controls.hpp"
#include "Palette.h"
#include "Canvas.hpp"

#include <olcPixelGameEngine.h>

Game::Game() :
		currentState(&drawState), drawState(32, 32, 512, 512) {
	RegisterControls();

}

bool Game::Update(Context* ctx, double dt) {
	return currentState->Update(ctx);
}

Game::~Game() {

}

void Game::SetGraphics(Context * ctx) {
	auto gfx = ctx->Gfx();
	gfx->KeyDown() = [=](Context * ctx, Key key, int mx, int my) {
		switch(key) {
			case F5:
			currentState = &drawState;
			break;
			case F6:
			currentState = &selectTileState;
			break;
			case F7:
			currentState = &levelEditState;
			break;
			default:
			break;
		}
		currentState->KeyPressed(ctx, key, mx, my);
	};
	gfx->KeyUp() = [&](Context * ctx, Key key, int mx, int my) {currentState->KeyReleased(ctx, key, mx, my);};
	gfx->MouseDown() = [&](Context * ctx, int mx, int my, int button) {
		currentState->MouseDown(ctx, mx, my,button);
	};
	gfx->MouseUp() = [&](Context * ctx, int mx, int my, int button) {currentState->MouseUp(ctx, mx, my, button);};
	gfx->TextEntered() = [&](Context * ctx, char c) {currentState->TextEntered(ctx, c);};
}
