/*
 * ControlFactory.cpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */
#include <iostream>
#include "ControlFactory.hpp"

// Keep a map over the different controls that are available and their names for use with factory functions
std::map<std::string, std::function<Control*(int, int, int, int,double,std::string)>> factories;

// Register a new factory function for a control
void RegisterFactory(std::string control,
		std::function<Control*(int, int, int, int,double value,const std::string& text)> generator) {
	if (factories.count(control) == 0) {
		factories.insert( { control, generator });
	}
}

// Factory method to create a control from string name. This is so we can load both built in and custom controls with parameters from text files
Control * Control_Create(std::string control, int x, int y, int w, int h,double value,const std::string& text) {
	if (factories.count(control) == 0) {
		std::cerr << "Error creating " << control << " no known factory. " << std::endl;
		return nullptr;
	} else {
		return factories.at(control)(x, y, w, h,value,text);
	}
}
