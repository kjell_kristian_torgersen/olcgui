/*
 * SFMLGraphics.cpp
 *
 *  Created on: Jul 30, 2019
 *      Author: kjell
 */

#include "SFMLGraphics.h"
#include "BitmapIO.hpp"
#include <iostream>
#include "Context.hpp"

static std::vector<uint32_t> upalette = { 0xFF000000U, 0xFFAA0000U, 0xFF00AA00U, 0xFFAAAA00U, 0xFF0000AAU, 0xFFAA00AAU, 0xFF0055AAU, 0xFFAAAAAAU, 0xFF555555U, 0xFFFF5555U,
		0xFF55FF55U, 0xFFFFFF55U, 0xFF5555FFU, 0xFFFF55FFU, 0xFF55FFFFU, 0xFFFFFFFFU, 0x0U };

void SFMLGraphics::UpdateSprites() {
	sf::Image image;
	auto pixels = new uint32_t[256 * 256];
	Bitmap bmp(256, 256);
	//for (int i = 0; i < 256 * 256; i++) {
	//	pixels[i] = 0xFF000000 | r;
	//}
	for (int i = 0; i < 256; i++) {
		int x = i % 16;
		int y = i / 16;
		for (int y0 = 0; y0 < 32; y0++) {
			for (int x0 = 0; x0 < 32; x0++) {
				bmp(32 * x + x0, 32 * y + y0) = sprites[i](x0, y0);
			}
		}
	}
	for (unsigned i = 0; i < bmp.Pixels().size(); i++) {
		pixels[i] = upalette[bmp.Pixels().at(i)];
	}
	image.create(256, 256, (unsigned char*) (pixels));
	texture.loadFromImage(image);
	sfmlsprites.clear();
	for (int y = 0; y < 16; y++) {
		for (int x = 0; x < 16; x++) {
			auto spr = sf::Sprite(texture, { 32 * x, 32 * y, 32, 32 });
			//auto spr = sf::Sprite(texture, { 0, 0, 256, 256 });
			//spr.setOrigin(0, 0);
			sfmlsprites.push_back(spr);
		}
	}
}

SFMLGraphics::SFMLGraphics(int width, int height, Game * game) :
		window(sf::VideoMode(width, height), "My window"), game(game), sprites(BitmapFromFile("sprites.bin")), ctx(this), palette(17) {

	int fps = 0;

	for (unsigned i = 0; i < palette.size(); i++)
		palette[i] = sf::Color(upalette[i] & 0xFF, (upalette[i] >> 8) & 0xFF, (upalette[i] >> 16) & 0xFF, (upalette[i] >> 24) & 0xFF);

	font.loadFromFile("/home/kjell/git/spriteedit4/Px437_IBM_VGA8.ttf");

	for (int i = 0; i < sf::Mouse::ButtonCount; i++) {
		mouse.push_back( { false, false, false });
	}

	for (int i = 0; i < NumberOfKeys; i++) {
		keys.push_back( { false, false, false });
	}
	window.setFramerateLimit(60);
	UpdateSprites();
	game->SetGraphics(&ctx);

	sf::Clock clock;
	sf::Clock clock2;
	// run the program as long as the window is open
	while (window.isOpen()) {
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		for (int i = 0; i < sf::Mouse::ButtonCount; i++) {
			mouse[i].bPressed = false;
			mouse[i].bReleased = false;
		}

		for (unsigned i = 0; i < keys.size(); i++) {
			keys[i].bPressed = false;
			keys[i].bReleased = false;
		}

		while (window.pollEvent(event)) {
			switch (event.type) {
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::MouseButtonPressed:
				mouse[event.mouseButton.button].bPressed = true;
				mouse[event.mouseButton.button].bHeld = true;
				if (mouseDown) {
					auto pos = sf::Mouse::getPosition(window);
					mouseDown(&ctx,pos.x, pos.y, event.mouseButton.button);
				}
				break;
			case sf::Event::MouseButtonReleased:
				mouse[event.mouseButton.button].bReleased = true;
				mouse[event.mouseButton.button].bHeld = false;
				if (mouseUp) {
					auto pos = sf::Mouse::getPosition(window);
					mouseUp(&ctx,pos.x, pos.y, event.mouseButton.button);
				}
				break;
			case sf::Event::TextEntered:
				if (textEntered != nullptr) {
					textEntered(&ctx,(char) event.text.unicode);
				}
				break;
			case sf::Event::KeyPressed:
				keys[event.key.code].bPressed = true;
				keys[event.key.code].bHeld = true;
				if (keyPressed != nullptr) {
					auto pos = sf::Mouse::getPosition(window);
					keyPressed(&ctx,(Key) event.key.code, pos.x, pos.y); // todo get which character that corresponds to key
				}
				break;
			case sf::Event::KeyReleased:
				keys[event.key.code].bReleased = true;
				keys[event.key.code].bHeld = false;
				if (keyReleased != nullptr) {
					auto pos = sf::Mouse::getPosition(window);
					keyReleased(&ctx,(Key) event.key.code, pos.x, pos.y); // todo get which character that corresponds to key
				}

				break;
			default:
				break;
			}

		}
		if (!game->Update(&ctx, clock2.restart().asSeconds())) {
			window.close();
		}

		//window.draw(sfmlsprites[0]);
		window.display();
		fps++;
		double t = clock.getElapsedTime().asSeconds();
		if (t >= 1.0) {
			//UpdateSprites();
			clock.restart();
			std::cout << fps / t << std::endl;
			fps = 0;
		}

	}
}

void SFMLGraphics::DrawPixel(int x, int y, int color) {
	sf::RectangleShape rs( { 1, 1 });
	rs.setPosition(x, y);
	rs.setFillColor(palette.at(color));
	window.draw(rs);

}

HWButton SFMLGraphics::GetKey(Key key) {
	return keys[key];
}

void SFMLGraphics::Clear(int color) {
	window.clear(palette.at(color));
}

void SFMLGraphics::DrawRect(int x, int y, int w, int h, int color, bool fill) {

	sf::RectangleShape rs( { (float) w, (float) h });
	rs.setPosition(x, y);
	if (fill) {
		rs.setFillColor(palette.at(color));
	} else {
		rs.setFillColor(palette.at(16));
		rs.setOutlineThickness(-1);
		rs.setOutlineColor(palette.at(color));
	}
	window.draw(rs);

	//rs.setFillColor(palette.at(color));
}

HWButton SFMLGraphics::GetMouse(int button) {
	return mouse[button];
	//auto pressed = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	//return {false, pressed, false};
}

void SFMLGraphics::DrawLine(int x0, int y0, int x1, int y1, int color) {
	sf::VertexArray va(sf::Lines, 2);
	va.append(sf::Vertex( { (float) x0, (float) y0 }, palette[color]));
	va.append(sf::Vertex( { (float) x1, (float) y1 }, palette[color]));
	window.draw(va);
}

void SFMLGraphics::DrawString(int x, int y, const std::string& text, int color) {
	sf::Text txt(text, font);
	txt.setFillColor(palette[color]);
	txt.setPosition(x, y - 8);
	txt.setOrigin(0, 0);
	txt.scale(0.5, 0.5);
	window.draw(txt);

}

void SFMLGraphics::DrawSprite(int x, int y, int sprite) {
	sfmlsprites[sprite].setPosition(x, y);
	window.draw(sfmlsprites[sprite]);
}

int SFMLGraphics::GetMouseX() {
	return sf::Mouse::getPosition(window).x;
}

int SFMLGraphics::GetMouseY() {
	return sf::Mouse::getPosition(window).y;
}

int SFMLGraphics::ScreenWidth() {
	return window.getSize().x;
}

int SFMLGraphics::ScreenHeight() {
	return window.getSize().y;
}

std::vector<Bitmap>& SFMLGraphics::Sprites() {
	return sprites;
}

std::function<void(Context*, Key, int, int)>& SFMLGraphics::KeyDown() {
	return keyPressed;
}

std::function<void(Context*, Key, int, int)>& SFMLGraphics::KeyUp() {
	return keyReleased;
}

std::function<void(Context*, int, int, int)>& SFMLGraphics::MouseDown() {
	return mouseDown;
}

std::function<void(Context*, int, int, int)>& SFMLGraphics::MouseUp() {
	return mouseUp;
}

std::function<void(Context*, char)>& SFMLGraphics::TextEntered() {
	return textEntered;
}

SFMLGraphics::~SFMLGraphics() {
	// TODO Auto-generated destructor stub
}

