
#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"

#include "PGE.hpp"
#include "PGEGraphics.hpp"

#include "Keys.hpp"



bool PGE::OnUserCreate() {
	return true;
}

bool PGE::OnUserUpdate(float fElapsedTime) {

	return gfx->Update(fElapsedTime);
}

PGE::PGE(int x, int y, PGEGraphics * gfx) : gfx(gfx) {
	Construct(x, y, 1, 1, false);
}

PGE::~PGE() {
	// TODO Auto-generated destructor stub
}

