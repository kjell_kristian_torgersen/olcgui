/*
 * PGEGraphics.cpp
 *
 *  Created on: Jul 29, 2019
 *      Author: kjell
 */

#include "PGEGraphics.hpp"
#include "BitmapIO.hpp"
#include <olcPixelGameEngine.h>

static std::vector<olc::Pixel> palette = { 0xFF000000, 0xFFAA0000, 0xFF00AA00, 0xFFAAAA00, 0xFF0000AA, 0xFFAA00AA, 0xFF0055AA, 0xFFAAAAAA, 0xFF555555, 0xFFFF5555, 0xFF55FF55,
		0xFFFFFF55, 0xFF5555FF, 0xFFFF55FF, 0xFF55FFFF, 0xFFFFFFFF, 0x0 };

static std::vector<Key> keytable = { Unknown, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, Num0, Num1, Num2, Num3, Num4, Num5, Num6, Num7, Num8,
		Num9, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, Up, Down, Left, Right, Space, Tab, LShift, LShift, Insert, Delete, Home, End, PageUp, PageDown, BackSpace, Escape,
		Return, Return, Pause, RSystem, Numpad0, Numpad1, Numpad2, Numpad3, Numpad4, Numpad5, Numpad6, Numpad7, Numpad8, Numpad9, Multiply, Divide, Add, Subtract, Period, };

PGEGraphics::PGEGraphics(int w, int h, Game * game) :
		pge(w, h, this), game(game), sprites(BitmapFromFile("sprites.bin")), ctx(this) {
	pge.SetPixelMode(olc::Pixel::MASK);
	game->SetGraphics(&ctx);
	pge.Start();

}

void PGEGraphics::DrawPixel(int x, int y, int color) {
	pge.Draw(x, y, palette.at(color));
}

HWButton PGEGraphics::GetKey(Key key) {
	int idx = (std::find(keytable.begin(), keytable.end(), key) - keytable.begin());

	auto but = pge.GetKey((enum olc::Key) idx);
	HWButton but2 = { but.bReleased, but.bPressed, but.bHeld };
	return but2;
}

void PGEGraphics::Clear(int color) {
	pge.Clear(palette.at(color));
}

void PGEGraphics::DrawRect(int x, int y, int w, int h, int color, bool fill) {
	if (fill) {
		pge.FillRect(x, y, w, h, palette.at(color));
	} else {
		pge.DrawRect(x, y, w, h, palette.at(color));
	}
}

HWButton PGEGraphics::GetMouse(int button) {
	auto but = pge.GetMouse(button);
	HWButton ret = { but.bReleased, but.bPressed, but.bHeld };
	return ret;
}

void PGEGraphics::DrawLine(int x0, int y0, int x1, int y1, int color) {
	pge.DrawLine(x0, y0, x1, y1, palette.at(color));
}

void PGEGraphics::DrawString(int x, int y, const std::string& text, int color) {
	pge.DrawString(x, y, text, palette.at(color));
}

int PGEGraphics::GetMouseX() {
	return pge.GetMouseX();
}

int PGEGraphics::GetMouseY() {
	return pge.GetMouseY();
}

bool PGEGraphics::Update(float fElapsedTime) {

	for (int i = olc::Key::A; i < olc::Key::NP_DECIMAL + 1; i++) {
		if (keyDown != nullptr) {
			if (pge.GetKey((olc::Key) i).bPressed) {
				keyDown(&ctx, keytable[i], pge.GetMouseX(), pge.GetMouseY());
			}
		}
		if (keyUp != nullptr) {
			if (pge.GetKey((olc::Key) i).bReleased) {
				keyUp(&ctx, keytable[i], pge.GetMouseX(), pge.GetMouseY());
			}
		}
	}

	if (mouseDown) {
		int button = 0;
		if (pge.GetMouse(button).bPressed) {
			mouseDown(&ctx, pge.GetMouseX(), pge.GetMouseY(), button);
		}
	}
	return game->Update(&ctx, fElapsedTime);
}

std::vector<Bitmap>& PGEGraphics::Sprites() {
	return sprites;
}

void PGEGraphics::DrawSprite(int x, int y, int sprite) {
	if (sprite < 0)
		sprite = 0;
	if (sprite >= sprites.size())
		sprite = sprites.size() - 1;
	auto& spr = sprites.at(sprite);
	for (int i = 0; i < spr.Height(); i++) {
		for (int j = 0; j < spr.Height(); j++) {
			pge.Draw(x + j, y + i, palette.at(spr(j, i)));
		}
	}
}

int PGEGraphics::ScreenWidth() {
	return pge.ScreenWidth();
}

int PGEGraphics::ScreenHeight() {
	return pge.ScreenHeight();
}

std::function<void(Context*, Key, int, int)>& PGEGraphics::KeyDown() {
	return keyDown;
}

std::function<void(Context*, Key, int, int)>& PGEGraphics::KeyUp() {
	return keyUp;
}

std::function<void(Context*, int, int, int)>& PGEGraphics::MouseDown() {
	return mouseDown;
}

std::function<void(Context*, int, int, int)>& PGEGraphics::MouseUp() {
	return mouseUp;
}

std::function<void(Context*, char)>& PGEGraphics::TextEntered() {
	return textEntered;
}

PGEGraphics::~PGEGraphics() {
	BitmapToFile("sprites.bin", sprites);
}

