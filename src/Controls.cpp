/*
 * Controls.cpp
 *
 *  Created on: Jul 26, 2019
 *      Author: kjell
 */
#include "Button.hpp"
#include "Frame.h"
#include "Label.h"
#include "Panel.hpp"
#include "ProgressBar.hpp"
#include "Control.hpp"
#include "ControlFactory.hpp"
#include "TextBox.hpp"
#include "Canvas.hpp"
#include "Palette.h"

#include <string>

// Factory functions for each built in control

Control * CreateButton(int x, int y, int w, int h, double value, const std::string& text) {
	return (Control*) new Button(x, y, w, h, text);
}

Control * CreateFrame(int x, int y, int w, int h, double value, const std::string& text) {
	return (Control*) new Frame(x, y, w, h);
}

Control * CreateLabel(int x, int y, int w, int h, double value, const std::string& text) {
	return (Control*) new Label(x, y, w, h, text);
}

Control * CreateTextBox(int x, int y, int w, int h, double value, const std::string& text) {
	return (Control*) new TextBox(x, y, w, h, text);
}


Control * CreatePanel(int x, int y, int w, int h, double value, const std::string& text) {
	return (Control*) new Panel(x, y, w, h);
}

Control * CreateProgressBar(int x, int y, int w, int h, double value, const std::string& text) {
	return (Control*) new ProgressBar(x, y, w, h, value);
}

Control * CreateCanvas(int x, int y, int w, int h, double value, const std::string& text) {
	return (Control*) new Canvas(x, y, w, h);
}

Control * CreatePalette(int x, int y, int w, int h, double value, const std::string& text) {
	return (Control*) new Palette(x, y, w, h);
}

// register all built in controls
void RegisterControls(void)
{
	RegisterFactory("button", CreateButton);
	RegisterFactory("frame", CreateFrame);
	RegisterFactory("label", CreateLabel);
	RegisterFactory("textbox", CreateTextBox);
	RegisterFactory("panel", CreatePanel);
	RegisterFactory("progressbar", CreateProgressBar);
	RegisterFactory("canvas", CreateCanvas);
	RegisterFactory("palette", CreatePalette);
}
