//#define OLC_PGE_APPLICATION
//#include "olcPixelGameEngine.h"

#include "Control.hpp"
#include "Button.hpp"
#include "ProgressBar.hpp"
#include "TextBox.hpp"
#include "Label.h"
#include "Frame.h"
#include "Panel.hpp"
#include "Palette.h"
#include "Canvas.hpp"

#include "Controls.hpp"
#include "ControlFactory.hpp"
#include <unistd.h>
#include "Game.hpp"

#define __PGE

#ifdef __PGE
#include "PGEGraphics.hpp"
#else
#include "SFMLGraphics.h"
#endif
int n = 20;
/*
// Override base class with your custom functionality
class PGE: public olc::PixelGameEngine {
public:
	PGE() {
		//sAppName = "PGE";
	}
	virtual ~PGE() {
		delete (panel);
	}
public:
	// controls are specified in a text file and generated
	// however it is nice to have direct access to some of the controls from here
	ProgressBar * progressBar = nullptr;
	Button * button = nullptr;
	TextBox * textBox = nullptr;
	Label * label = nullptr;
	Panel* panel = nullptr;

	bool OnUserCreate() override
	{
		//SetPixelMode(olc::Pixel::ALPHA);
		// register built in controls
		RegisterControls();
		// optionally register own controls by calling the RegisterFactory() function in ControlFactory.cpp
		// then they are automatically recognized by Panel::FromFile()

		// read in controls from text file
		panel = Panel::FromFile("guitest.txt");
		Palette* palette = (Palette*)panel->GetControl("palette1");
		Canvas* canvas = (Canvas*)panel->GetControl("canvas1");
		canvas->cpalette = palette;
		label = (Label*)panel->GetControl("label1");

		// access the button specified by the text file
		button = (Button*) panel->GetControl("button1");

		// what to do when button1 is clicked (lambda function)
		button->clicked = [](Control* sender,int x, int y) {
			// print mouse position x,y when clicked
				std::cout << "button1: mouse clicked as position: " << x << " " << y << std::endl;
			};

		// what to do when button2 is clicked (lambda function)
		panel->GetControl("button2")->clicked = [](Control* sender,int x, int y)
		{
			if(sender->text != "test") {
				sender->text ="test";
			} else {
				sender->text ="button2";
			}
		};

		// access the progress bar (for changing its value later)
		progressBar = (ProgressBar*) panel->GetControl("progressbar1");

		// access the text box
		textBox = (TextBox*) panel->GetControl("textbox1");

		// access the label
		label = (Label*) panel->GetControl("label1");

		// Called once at the start, so create things here
		return true;
	}

	bool OnUserUpdate(float fElapsedTime) override
	{
		// clear screen
		//Clear(3);

		// set the progress bar to match how far to the right the mouse pointer is,
		// just to see that the mouse button works
		//progressBar->value = (double) GetMouseX() / (double) ScreenWidth();
		// update all controls, both handling mouse and drawing.
		//panel->Update(this);

		// limit frame rate to max 100 fps
		usleep(1000);
		return true;
	}
};*/

int main() {
	Game game;
#ifdef __PGE
	PGEGraphics pge(1024,768, &game);
#else
	SFMLGraphics sfml(1024,768, &game);
#endif
	return 0;
}
