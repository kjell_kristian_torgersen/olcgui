/*
 * SelectTileState.cpp
 *
 *  Created on: Aug 3, 2019
 *      Author: kjell
 */

#include "SelectTileState.hpp"

SelectTileState::SelectTileState() {
	// TODO Auto-generated constructor stub

}

bool SelectTileState::Update(Context* ctx) {
	ctx->Gfx()->Clear(2);
	int mx = ctx->Gfx()->GetMouseX();
	int my = ctx->Gfx()->GetMouseY();
	for (int y = 0; y < 16; y++) {
		for (int x = 0; x < 16; x++) {
			ctx->Gfx()->DrawSprite(32 * x, 32 * y, 16 * y + x);
		}
	}
	ctx->Gfx()->DrawRect(mx & ~31, my & ~31, 32, 32, 4, false);
	ctx->Gfx()->DrawRect(32 * (ctx->CurrentTile() % 16), 32 * (ctx->CurrentTile() / 16), 32, 32, 13, false);

	return true;
}

void SelectTileState::KeyPressed(Context * ctx, Key key, int mx, int my) {
}

void SelectTileState::KeyReleased(Context * ctx, Key key, int mx, int my) {
}

void SelectTileState::MouseDown(Context * ctx, int mx, int my, int button) {

	int x = mx / 32;
	int y = my / 32;

	if (button == 0) {
		if (x >= 0 && x < 16) {
			if (y >= 0 && y < 16) {
				ctx->CurrentTile() = 16 * y + x;
			}
		}
	}
}

void SelectTileState::MouseUp(Context * ctx, int mx, int my, int button) {
}

void SelectTileState::TextEntered(Context * ctx, char c) {
}

SelectTileState::~SelectTileState() {
	// TODO Auto-generated destructor stub
}

