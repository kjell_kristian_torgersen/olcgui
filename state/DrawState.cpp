/*
 * DrawState.cpp
 *
 *  Created on: Aug 3, 2019
 *      Author: kjell
 */

#include "DrawState.hpp"
#include "BitmapIO.hpp"

bool DrawState::Update(Context* ctx) {
	auto& bmp = ctx->Gfx()->Sprites()[ctx->CurrentTile()];
	auto olc = ctx->Gfx();
	// calculate grid size
	//auto& bmp = olc->Sprites().at(currentImage);
	int stepx = w0 / bmp.Width();
	int stepy = h0 / bmp.Height();
	// get which pixel mouse pointer is over
	int px = (olc->GetMouseX() - x0) / stepx;
	int py = (olc->GetMouseY() - y0) / stepy;
	olc->Clear(1);
	// draw the canvas (background)
	olc->DrawRect(x0, y0, bmp.Width() * stepx, bmp.Height() * stepy, 7, true);
	// draw all pixels
	for (int y = 0; y < bmp.Height(); y++) {
		for (int x = 0; x < bmp.Width(); x++) {
			olc->DrawRect(x0 + x * stepx, y0 + y * stepy, stepx - 1, stepy - 1, bmp(x, y), true);
		}
	}

	palette.Update(ctx);
	// if a palette is "connected" to this canvas, enable drawing with the palette color
	//if (cpalette != nullptr) {
	if (olc->GetMouse(0).bPressed) {
		undo.push(bmp);
	}
	if (olc->GetMouse(0).bHeld) {
		if ((px >= 0) && (px < bmp.Width()) && (py >= 0) && (py < bmp.Height())) {
			bmp(px, py) = palette.color;
		}
	}

	if(olc->GetKey(F).bPressed) {
		Fill(bmp, px, py, (char)bmp(px,py), (char)palette.color);
	}

	//}
	// just draw a frame around the canvas
	//DrawFrame(ctx, x0, y0, bmp.Width() * stepx, bmp.Height() * stepy, 1);
	// use a label to show which pixel position cursor is above
	label.text = "(" + std::to_string(px) + ", " + std::to_string(py) + ")";
	label.Update(ctx);
	return true;
	//DrawBitmap(ctx, bmp, 0, 0);
}

void DrawState::KeyPressed(Context * ctx, Key key, int mx, int my) {

}

void DrawState::KeyReleased(Context * ctx, Key key, int mx, int my) {
}

void DrawState::MouseDown(Context * ctx, int mx, int my, int button) {
}

void DrawState::MouseUp(Context * ctx, int mx, int my, int button) {
}

void DrawState::TextEntered(Context * ctx, char c) {
}

DrawState::DrawState(int x, int y, int w, int h, int imageWidth, int imageHeight) :
		x0(x), y0(y), w0(w), h0(h), bmp(imageWidth, imageHeight), palette(x + w, y, 64, h), label(x, y - 16, w, 16, "") {
	label.color = 15;
}

DrawState::~DrawState() {
	// TODO Auto-generated destructor stub
}

