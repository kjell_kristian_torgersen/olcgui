/*
 * LevelEditState.cpp
 *
 *  Created on: Aug 3, 2019
 *      Author: kjell
 */

#include "LevelEditState.hpp"

LevelEditState::LevelEditState() {
	// TODO Auto-generated constructor stub

}

bool LevelEditState::Update(Context* ctx) {
	ctx->Gfx()->Clear(4);
	int mx = ctx->Gfx()->GetMouseX();
	int my = ctx->Gfx()->GetMouseY();

	for (int y = 0; y < ctx->Gfx()->ScreenHeight() / 32; y++) {
		for (int x = 0; x < ctx->Gfx()->ScreenWidth() / 32; x++) {
			auto rr = [=](int x0, int y0) -> bool {
				return ctx->Map().Road(x0+x, y0+y);
			};
			ctx->Gfx()->DrawSprite(32 * x, 32 * y, ctx->Map()(x, y));
			if (rr(0, 0)) {
				if (rr(-1, 0) && rr(+1, 0) && rr(0, -1) && rr(0, +1)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 22);
				} else if (rr(-1, 0) && rr(+1, 0) && rr(0, +1)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 6);
				} else if (rr(-1, 0) && rr(+1, 0) && rr(0, -1)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 38);
				} else if (rr(0, -1) && rr(0, +1) && rr(1, 0)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 21);
				} else if (rr(0, -1) && rr(0, +1) && rr(-1, 0)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 23);
				} else if (rr(-1, 0) && rr(+1, 0)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 4);
				} else if (rr(0, -1) && rr(0, +1)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 20);
				} else if (rr(0, +1) && rr(+1, 0)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 5);
				} else if (rr(+1, 0) && rr(0, -1)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 37);
				} else if (rr(-1, 0) && rr(0, +1)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 7);
				} else if (rr(-1, 0) && rr(0, -1)) {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 39);
				} else {
					ctx->Gfx()->DrawSprite(32 * x, 32 * y, 22);
				}
			}
		}
	}
	if (ctx->Gfx()->GetMouse(1).bPressed) {
		ctx->CurrentTile() = ctx->Map()(mx / 32, my / 32);
	}
	if (ctx->Gfx()->GetMouse(0).bHeld || ctx->Gfx()->GetMouse(0).bPressed) {
		ctx->Map()(mx / 32, my / 32) = ctx->CurrentTile();
	}
	if (ctx->Gfx()->GetKey(R).bPressed || ctx->Gfx()->GetKey(R).bHeld) {
		ctx->Map().SetRoad(mx / 32, my / 32, true);
	}
	if (ctx->Gfx()->GetKey(E).bPressed || ctx->Gfx()->GetKey(E).bHeld) {
		ctx->Map().SetRoad(mx / 32, my / 32, false);
	}
	ctx->Gfx()->DrawRect(mx & ~31, my & ~31, 32, 32, 12, false);

	return true;
}

void LevelEditState::KeyPressed(Context * ctx, Key key, int mx, int my) {
	switch(key) {
	case F:
		ctx->Map().Fill(mx/32, my/32, ctx->CurrentTile());
		break;
	default:
		break;
	}
}

void LevelEditState::KeyReleased(Context * ctx, Key key, int mx, int my) {
}

void LevelEditState::MouseDown(Context * ctx, int mx, int my, int button) {

}

void LevelEditState::MouseUp(Context * ctx, int mx, int my, int button) {
}

void LevelEditState::TextEntered(Context * ctx, char c) {
}

LevelEditState::~LevelEditState() {
	// TODO Auto-generated destructor stub
}

